-----------------------------------------------------------------------------------------
--
-- level1.lua
--
-----------------------------------------------------------------------------------------

system.activate("multitouch")

local storyboard = require( "storyboard" )
local scene = storyboard.newScene()

local movieclip = require "movieclip"

-- include Corona's "physics" library
local physics = require "physics"
physics.start(); physics.pause()

-- physics.setDrawMode('hybrid')
physics.setGravity(0, 0)

--------------------------------------------

-- forward declarations and other locals
screenW, screenH, halfW = display.contentWidth, display.contentHeight, display.contentWidth*0.5

-- require "events"
-- require "players"

group = nil

local bunkerW, bunkerH = 105, 93
local enemies = {}

-----------------------------------------------------------------------------------------
-- BEGINNING OF YOUR IMPLEMENTATION
-- 
-- NOTE: Code outside of listener functions (below) will only be executed once,
--		 unless storyboard.removeScene() is called.
-- 
-----------------------------------------------------------------------------------------

function destroyEnemy( enemy )
	local function removeEnemy()
		enemy.parent.name = ""
		enemy.parent.alpha = 0
	end

	enemy.parent.isBodyActive = false
	-- enemy.parent:setLinearVelocity(0, 0)
	enemy.alpha = 0
	enemy.label.alpha = 0
	local explosion = display.newImageRect("i/explosion.png", 130, 85)
	explosion:scale(0.5, 0.5)
	explosion.alpha = 0.1
	
	enemy.parent:insert(explosion)

	transition.to(explosion, {time=200, alpha=0.5, xScale=1, yScale=1, y=explosion.y-20})
	transition.to(explosion, {delay=200, time=200, alpha=0.1, xScale=0.5, yScale=0.5, onComplete=removeEnemy})
end

function decrHp( enemy )
	enemy.hp = enemy.hp - enemy.onFire
	enemy.label.text = enemy.hp
	if enemy.hp < 0 then
		destroyEnemy(enemy)
	end
end

function collisionBunker( self, e )
	if e.phase == "began" then
		if e.other.name == "enemy" then
			local enemy = e.other[1]
			enemy.onFire = enemy.onFire + 1
			
			local g = e.target.bunker
			if not g.onFire then
				g.onFire = true
				-- local fire = display.newImageRect("i/fire.png", 20, 20)
				local fire = movieclip.newAnim({"i/fire.png", "i/fire1.png"}, 20, 20)
				fire.x, fire. y = g.x, g.y - bunkerH/2 - 15
				group:insert(fire)
				fire:play()
			end
		end
	elseif e.phase == "ended" then
		if e.other.name == "enemy" then
			local enemy = e.other[1]
			enemy.onFire = enemy.onFire - 1
		end
	end
end

function frameHandler( e )
	for i = 1, #enemies, 1 do
		if enemies[i].alpha > 0 and enemies[i][1].onFire > 0 then
			decrHp(enemies[i][1])
		end
	end

	-- for i = group.numChildren, 1, -1 do
	-- 	local g = group[i]
	-- 	if g.name == "enemy" then
	-- 		if g.hp < 0 then
	-- 			g:removeSelf()
	-- 		end
	-- 	end
	-- end
end

function addEnemy( e )
	local eGroup = display.newGroup()
	eGroup.name = "enemy"

	local enemy = display.newImageRect("i/tank.png", 100, 100)
	-- enemy.x, enemy.y = math.random(50, screenW-50), 100
	-- enemy.name = "enemy"
	enemy.hp = 50
	enemy.onFire = 0

	local label = display.newText(enemy.hp, enemy.x+20, enemy.y-80)
	enemy.label = label

	eGroup:insert(enemy)
	eGroup:insert(label)

	table.insert(enemies, eGroup)

	eGroup:setReferencePoint(display.CenterReferencePoint)
	eGroup.x, eGroup.y = math.random(50, screenW-50), 100

	physics.addBody(eGroup, "dynamic", {radius=50})
	eGroup:applyLinearImpulse(0, 0.5, eGroup.x, eGroup.y)
end

function addBunker( e )
	if e.phase == "began" then
		if e.target.name == "bunkerO" then
			return true
		end

		local x, y = e.x, e.y
		local bunker = display.newImageRect("i/bunker.png", bunkerW, bunkerH)
		bunker.x, bunker.y = x, y
		bunker.name = "bunker"
		physics.addBody(bunker, "static", {radius=bunkerW/2})
		group:insert(bunker)

		local bunkerOverlay = display.newCircle(x, y, bunkerW*2)
		bunkerOverlay.name = "bunkerO"
		bunkerOverlay.bunker = bunker
		bunkerOverlay.alpha = 0.1
		physics.addBody(bunkerOverlay, "static", {radius=bunkerW*2})
		bunkerOverlay.isSensor = true
		group:insert(bunkerOverlay)

		bunkerOverlay:addEventListener('touch', addBunker)
		bunkerOverlay.collision = collisionBunker
		bunkerOverlay:addEventListener( "collision",  bunkerOverlay)
	end
end

-- Called when the scene's view does not exist:
function scene:createScene( event )
	group = self.view

	local g = graphics.newGradient(
	  { 80, 120, 80 },
	  { 60, 100, 60 },
	  "down" )

	local bg = display.newRect( 0, 0, screenW, screenH)
	bg:setFillColor( g )
	bg.alpha = 1
	group:insert(bg)
	bg:addEventListener('touch', addBunker)

	

	-- Timers
	timer.performWithDelay(2000, addEnemy, 0 )

	-- Frame handlers
	Runtime:addEventListener( "enterFrame", frameHandler )
end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local group = self.view
	
	physics.start()
	
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view
	
	physics.stop()
	
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
	local group = self.view
	
	package.loaded[physics] = nil
	physics = nil
end

-----------------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
-----------------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-----------------------------------------------------------------------------------------

return scene